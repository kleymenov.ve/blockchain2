const HDWalletProvider = require("@truffle/hdwallet-provider");
const mnemonic = "route obscure pyramid chief spider wet drum fee advance vanish spice embark";

module.exports = {
  // Uncommenting the defaults below 
  // provides for an easier quick-start with Ganache.
  // You can also follow this format for other networks;
  // see <http://truffleframework.com/docs/advanced/configuration>
  // for more details on how to specify configuration options!
  //
  networks: {
    kovan: {
      provider: function() {
        return new HDWalletProvider(mnemonic, "https://kovan.infura.io/v3/47546b794b014a3fade5ba6a4792cb74")
      },
      network_id: '42',
    },
  },
  //
};
