pragma solidity >=0.4.22 <0.9.0;
import "./TokenOBT.sol";

contract RockPaperScissors
{
    TokenOBT public Token;
    string[] figures;
    address player1;
    address player2;
    string[] public choices1;
    string[] public choices2;
    address winner;
    int winnerNumber;
    int[] winnerEach;
    uint256 BID = uint256(1000);

    event winnerEvent(int winnerNumber);
    event playEvent(address user);
    event gamesCount(uint count, address user);

    modifier isRegistered()
    {
        require(msg.sender != player1 && msg.sender != player2);
        _;
    }

    function RegMe() isRegistered() public {
        if (int(player1) == 0){
            player1 = msg.sender;
            Token.transferFrom(msg.sender, address(this), BID);
        } else if (int(player2) == 0) {
            player2 = msg.sender;
            Token.transferFrom(msg.sender, address(this), BID);
        }
    }

    constructor(address tokenAddr) public
    {
        figures = ["rock", "paper", "scissors", "rock", "paper"];
        Token = TokenOBT(tokenAddr);
    }

    function compareStringsbyBytes(string memory s1, string memory s2) private pure returns(bool){
        return keccak256(abi.encodePacked(s1)) == keccak256(abi.encodePacked(s2));
    }


    function MakeDecision (string memory player1Figure, string memory player2Figure) view private returns (int win) {
        for (uint i = 1; i < 4; i++) {
            if (compareStringsbyBytes(player1Figure, figures[i])) {
                if  (compareStringsbyBytes(player2Figure, figures[i - 1])) {
                    win = 1;
                    break;
                }

                if  (compareStringsbyBytes(player2Figure, figures[i + 1])) {
                    win = 2;
                    break;
                }

                if  (compareStringsbyBytes(player2Figure, figures[i])) {
                    win = 0;
                    break;
                }
            } else continue;
        }
    }

    function Play(string memory choice) public
    {
        if (msg.sender == player1) {
            choices1.push(choice);
            emit playEvent(player1);
            emit gamesCount(choices1.length, player1);
            if (choices1.length == 0) {
                Token.approve(player2, BID);
            }
        }
        else if (msg.sender == player2) {
            choices2.push(choice);
            emit playEvent(player2);
            emit gamesCount(choices2.length, player2);
            if (choices2.length == 0) {
                Token.approve(player1, BID);
            }
        }

        if (choices1.length == 3 && choices2.length == 3) {
            int first = 0;
            int second = 0;
            for(uint i = 0; i < 3; i++){
                winnerEach.push(MakeDecision(choices1[i], choices2[i]));
                if (winnerEach[i] == 1)
                    first++;
                else if (winnerEach[i] == 2)
                    second++;
            }
            if (first == second) {
                winner = address(0);
                winnerNumber = 0;
                emit winnerEvent(0);
                Token.transfer(player1, BID);
                Token.transfer(player2, BID);
            }
            else if (first > second) {
                winner = player1;
                winnerNumber = 1;
                emit winnerEvent(1);
                Token.transfer(player1, BID * 2);
            } else {
                winner = player2;
                winnerNumber = 2;
                emit winnerEvent(2);
                Token.transfer(player2, BID * 2);
            }
        }

    }

    function GetResult() public view returns (int) {
        return winnerNumber;
    }

    function GetResult(uint256 game) public view returns (int) {
        return winnerEach[game];
    }

    function NewGame() public {
        delete player1;
        delete player2;
        delete choices1;
        delete choices2;
        winner = address(0);
        delete winnerEach;
    }

    function WhoAmI() public view returns (uint number) {
        if (msg.sender == player1) number = 1;
        else if (msg.sender == player2) number = 2;
    }
}
