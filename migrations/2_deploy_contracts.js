const RockPaperScissors = artifacts.require("RockPaperScissors");
const TokenOBT = artifacts.require("TokenOBT");

module.exports = function(deployer) {
  deployer.deploy(TokenOBT)
    .then((Token) => {
      return deployer.deploy(RockPaperScissors, Token.address);
    });
};
