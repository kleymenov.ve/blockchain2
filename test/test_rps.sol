pragma solidity >=0.4.25 <0.6.0;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/RockPaperScissors.sol";
import "../contracts/TokenOBT.sol";


contract TestRockPaperScissors {
  function testInitialBalanceUsingDeployedContract() public {
    TokenOBT token = TokenOBT(DeployedAddresses.TokenOBT());

    uint expected = 10000;

    Assert.equal(token.balanceOf(tx.origin), expected, "Owner should have 10000 MetaCoin initially");
  }
}
