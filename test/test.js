require('mocha');
const { assert } = require('chai');

const RockPaperScissors = artifacts.require('RockPaperScissors');
const TokenOBT = artifacts.require('TokenOBT');


contract('RockPaperScissors', (accounts) => {
  let instance;
  let instanceToken;
  let instanceAddress;
  let tokenAddress;
  const playerOne = accounts[0];
  const playerTwo = accounts[1];

  const choicesOne = ['rock', 'paper', 'scissors'];
  const choicesTwo = ['scissors', 'rock', 'paper'];

  before(async () => {
    instance = await RockPaperScissors.deployed();
    instanceAddress = instance.address;
    instanceToken = await TokenOBT.deployed();
    tokenAddress = instanceToken.address;
  });

  describe('First publication', () => {
    it('should put all 10000 tokens into 1st account balance', async () => {
      const balance = await instanceToken.balanceOf(playerOne);
      assert.equal(balance.valueOf(), 10000);
    });
  });

  describe('Transfer before game', () => {
    it('should transfer 5000 tokens on the 2nd players account', async () => {
      await instanceToken.transfer(accounts[1], 5000);
      const balance1 = await instanceToken.balanceOf(playerOne);
      const balance2 = await instanceToken.balanceOf(playerTwo);
      assert.equal(balance1.valueOf(), 5000);
      assert.equal(balance2.valueOf(), 5000);
    });
  });

  describe('Approve', () => {
    it('should approve 1000 to games contract', async () => {
      await instanceToken.approve(instanceAddress, 1000);
      await instanceToken.approve(instanceAddress, 1000, { from: accounts[1] });
      const allowance1 = await instanceToken.allowance(playerOne, instanceAddress);
      const allowance2 = await instanceToken.allowance(playerTwo, instanceAddress);
      assert.equal(allowance1.valueOf(), 1000);
      assert.equal(allowance2.valueOf(), 1000);
    });
  });

  describe('Gameplay', () => {
    it('should register the players', async () => {
      await instance.RegMe();
      await instance.RegMe({ from: playerTwo });
      const playerOneNumber = await instance.WhoAmI();
      const playerTwoNumber = await instance.WhoAmI({ from: playerTwo });
      assert.equal(playerOneNumber, 1);
      assert.equal(playerTwoNumber, 2);
    });
    it('should make a choice', async () => {
      for (let choice in choicesOne) {
        await instance.Play(choice);
      }
      for (let choice in choicesTwo) {
        await instance.Play(choice, { from: playerTwo });
      }
    });
    it('should print right winner', async () => {
      const correctResults = [1, 2, 1];
      Promise.allSettled([
        instance.GetResult(0),
        instance.GetResult(1),
        instance.GetResult(2),
      ])
        .then((gameResults) => {
          const values = gameResults.value;
          for (let i = 0; i < 3; i++) {
            assert.equal(values[i], correctResults[i]);
          }
        })
        .then(async () => {
            const res = await instance.GetResult();
            assert.equal(res.valueOf(), 1);
        });
    });
  });

  describe('Balance check', () => {
    it('checks if the winner got the money and the loser sent them', async () => {
      instanceToken.balanceOf(playerOne).then((balance1) => {
        assert.equal(balance1, 6000);
      });
      instanceToken.balanceOf(playerTwo).then((balance2) => {
        assert.equal(balance2, 4000);
      });
    });
  });
});